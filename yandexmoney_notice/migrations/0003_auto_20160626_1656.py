# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yandexmoney_notice', '0002_auto_20160626_1401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='yadtransaction',
            name='codepro',
            field=models.BooleanField(default=False, verbose_name='Защита кодом протекции'),
        ),
        migrations.AlterField(
            model_name='yadtransaction',
            name='test_notification',
            field=models.BooleanField(default=False, verbose_name='Тестовое уведомление'),
        ),
    ]
